<?php
 
/*
 * Following code will get single product details
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
$host="localhost";      //replace with your hostname 
$username="root";       //replace with your username 
$password="";           //replace with your password 
$db_name="android";     //replace with your database 

//open connection to mysql db
$connection = new mysqli($host,$username,$password,$db_name);

//set http header with content type as JSON and encoded with utf-8
header("Content-Type: application/json;charset=utf-8");

// check connection 
if (mysqli_connect_errno()) {
	$response["success"] = 0;
	$response["message"] = "Error Connection failed " .mysqli_connect_error();
	// echoing JSON response
	echo json_encode($response);
	exit();		
}

if(!$connection->set_charset("utf8")) {  
	$response["success"] = 0;
	$response["message"] = "Error loading character set utf8:". $connection->error;

	// echoing JSON response
	echo json_encode($response);
	//close the db connection
	$connection->close();
	exit(); 
}
 
// check for post data
if (!empty($_GET["pid"])) {
    $pid = $_GET['pid'];
 
    // get a product from products table
	$sql = "SELECT *FROM products WHERE pid = $pid";
	$result = $connection->query($sql);
 
    if (!empty($result)) {
        // check for empty result
        if ($result->num_rows > 0) {
 
            $result = $result->fetch_assoc();
 
            $product = array();
            $product["pid"] = $result["pid"];
            $product["name"] = $result["name"];
            $product["price"] = $result["price"];
            $product["description"] = $result["description"];
            // success
            $response["success"] = 1;
 
            // user node
            $response["product"] = array();
 
            array_push($response["product"], $product);
 
            // echoing JSON response
            echo json_encode($response);
        } else {
            // no product found
            $response["success"] = 0;
            $response["message"] = "No product found";
 
            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // no product found
        $response["success"] = 0;
        $response["message"] = "No product found";
 
        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}

//close the db connection
$connection->close();
?>